use criterion::{criterion_group, criterion_main, Criterion};

use swiss_train_challenge::models::Context;
use swiss_train_challenge::INPUT_DATA_FILENAME;

pub fn dijkstra_through_cantons_benchmark(c: &mut Criterion) {
    let context = Context::from_file(INPUT_DATA_FILENAME);
    let find_canton = |name| {
        context
            .cantons
            .iter()
            .find(|c| c.name == name)
            .expect(format!("canton id for {}", name).as_str())
            .id
    };

    let cantons = vec![
        find_canton("Vaud"),
        find_canton("Fribourg"),
        find_canton("Bern"),
        find_canton("Luzern"),
        find_canton("Schwyz"),
        find_canton("Uri"),
        find_canton("Ticino"),
        find_canton("Graubünden"),
        find_canton("Zürich"),
    ];

    let mut group = c.benchmark_group("find_route_through_cantons");
    group.sample_size(10);
    group.bench_function("route through cantons", |b| {
        b.iter(|| {
            context
                .find_route_through_cantons(&cantons)
                .expect("a route")
        })
    });
}

criterion_group!(benches, dijkstra_through_cantons_benchmark);
criterion_main!(benches);
