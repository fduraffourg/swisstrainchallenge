@0x89056142ec556327;

struct InputData {
  cantons @0 :List(Canton);
  stops @1 :List(Stop);
  trips @2 :List(Trip);
  cantonsNeighbors @3 :List(CantonNeighbors);
}

struct Canton {
  id @0 :UInt64;
  name @1: Text;
}

struct CantonNeighbors {
  canton @0 :UInt64;
  neighbors @1 :List(UInt64);
}

struct Stop {
  id @0 :UInt64;
  name @1: Text;
  canton @2 :UInt64;
  transfers @3 :List(StopTransfer);
}

struct StopTime {
  arrivalTime @0 :UInt32;
  departureTime @1 :UInt32;
  stop @2 :UInt64;
}

struct Trip {
  id @0 :UInt64;
  name @1: Text;
  stopTimes @2: List(StopTime);
}

struct StopTransfer {
  to @0 :UInt64;
  time @1 :UInt32;
}

struct Generation {
  generation @0 :UInt32;
  individuals @1 :List(Individual);
  all @2 :List(Individual);
}

struct Individual {
  visitedCantons @0 :List(UInt64);
  duration @1 :UInt32;
}
