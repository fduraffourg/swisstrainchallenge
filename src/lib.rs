pub mod preprocess;
pub mod models;
pub mod algos;

pub const TARGET_DATE: (i32, u32, u32) = (2024, 10, 12);
pub const INPUT_DATA_FILENAME: &str = "resources/input_data.capnp";
// pub const START_STOP_NAME: &str = "Renens VD";
// pub const START_STOP_NAME: &str = "Genève, gare Cornavin";

pub mod models_capnp {
    include!(concat!(env!("OUT_DIR"), "/schemas/models_capnp.rs"));
}

