use std::{
    collections::{HashMap, HashSet},
    path::Path,
    process::Command,
    sync::Arc,
};

use gtfs_structures::Gtfs;
use swiss_train_challenge::{models::CantonId, models::StopId, preprocess::xtf};

// https://opentransportdata.swiss/en/dataset/timetable-2024-gtfs2020
const URL: &str = "https://opentransportdata.swiss/dataset/7cc000ea-0973-40c1-a557-7971a4939da3/resource/4fb97c3a-a7bc-46f0-9adc-6707d9307133/download/gtfs_fp2024_2024-10-28.zip";
const GTFS_FILE: &str = "resources/gtfs.zip";

// From https://www.swisstopo.admin.ch/fr/modele-du-territoire-swissboundaries3d
const BORDERS_URL: &str = "https://data.geo.admin.ch/ch.swisstopo.swissboundaries3d/swissboundaries3d_2024-01/swissboundaries3d_2024-01_2056_5728.xtf.zip";
const BORDERS_ZIP_FILE: &str = "resources/borders.xtf.zip";
const BORDERS_FILE: &str = "resources/swissBOUNDARIES3D_1_5.xtf";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let resources_folder = Path::new("resources");
    if !resources_folder.exists() {
        println!("Create resources folder");
        std::fs::create_dir(resources_folder).expect("create resources folder");
    }

    let gtfs_path = Path::new(GTFS_FILE);
    if !gtfs_path.exists() {
        println!("Gtfs file not found, download it");
        Command::new("curl")
            .arg("-o")
            .arg(GTFS_FILE)
            .arg(URL)
            .spawn()
            .expect("failed to start download of gtfs file")
            .wait()
            .expect("failed to download gtfs file");
    } else {
        println!("Gtfs file found, skip download");
    }

    let borders_path = Path::new(BORDERS_FILE);
    if !borders_path.exists() {
        println!("Borders file not found, download it");
        Command::new("curl")
            .arg("-o")
            .arg(BORDERS_ZIP_FILE)
            .arg(BORDERS_URL)
            .spawn()
            .expect("failed to start download of borders file")
            .wait()
            .expect("failed to download borders file");
        Command::new("unzip")
            .arg(BORDERS_ZIP_FILE)
            .arg("-d")
            .arg("resources")
            .arg("swissBOUNDARIES3D_1_5.xtf")
            .spawn()
            .expect("failed to unzip borders file")
            .wait()
            .expect("failed to unzip borders file");
    } else {
        println!("Borders file found, skip download");
    }

    let cantons = {
        let content = std::fs::read_to_string(BORDERS_FILE).expect("read xtf file");
        xtf::parse_xtf(&content).expect("parse xtf content")
    };
    println!("Cantons borders have been read");

    let gtfs = Gtfs::from_path(GTFS_FILE)?;
    gtfs.print_stats();

    let target_date = chrono::naive::NaiveDate::from_ymd_opt(
        swiss_train_challenge::TARGET_DATE.0,
        swiss_train_challenge::TARGET_DATE.1,
        swiss_train_challenge::TARGET_DATE.2,
    )
    .expect("target date");

    // Generate the stop list
    println!("Generate the stop list");
    let bar = indicatif::ProgressBar::new(gtfs.stops.len() as u64);
    let mapped_stops: Vec<(StopId, CantonId, Arc<gtfs_structures::Stop>)> = gtfs
        .stops
        .iter()
        .enumerate()
        .filter_map(|(idx, (_, stop))| {
            bar.inc(1);
            cantons
                .get_canton_of(stop)
                .map(|canton| (idx as u32, canton.num, stop.clone()))
        })
        .collect();
    let stop_ids: HashMap<&str, StopId> = mapped_stops
        .iter()
        .map(|(id, _, stop)| (stop.id.as_str(), *id))
        .collect();
    let stop_cantons: HashMap<&str, CantonId> = mapped_stops
        .iter()
        .map(|(_, canton, stop)| (stop.id.as_str(), *canton))
        .collect();
    let mut stops: Vec<Arc<swiss_train_challenge::models::Stop>> =
        Vec::with_capacity(gtfs.stops.len());
    for (id, canton, stop) in &mapped_stops {
        stops.push(Arc::new(swiss_train_challenge::models::Stop {
            id: *id,
            name: stop.name.to_owned(),
            canton: *canton,
            transfers: stop
                .transfers
                .iter()
                .flat_map(|t| {
                    stop_ids.get(t.to_stop_id.as_str()).map(|to| {
                        swiss_train_challenge::models::StopTransfer {
                            to: *to,
                            time: t.min_transfer_time.unwrap(),
                        }
                    })
                })
                .collect(),
        }));
    }
    let stops_by_id: HashMap<StopId, Arc<swiss_train_challenge::models::Stop>> =
        stops.iter().map(|s| (s.id, s.clone())).collect();
    bar.finish();

    println!("Find number of routes passing at each stop");
    let bar = indicatif::ProgressBar::new(gtfs.trips.len() as u64);
    let mut stop_routes: HashMap<&str, Arc<HashSet<&str>>> = HashMap::new();
    for (_, trip) in &gtfs.trips {
        bar.inc(1);
        for st in &trip.stop_times {
            match stop_routes.get_mut(st.stop.id.as_str()) {
                Some(routes) => {
                    Arc::get_mut(routes).unwrap().insert(trip.route_id.as_str());
                }
                None => {
                    let mut routes = Arc::new(HashSet::new());
                    Arc::get_mut(&mut routes)
                        .unwrap()
                        .insert(trip.route_id.as_str());
                    stop_routes.insert(st.stop.id.as_str(), routes);
                }
            }
        }
    }
    let stops_connections: HashMap<&str, usize> = stop_routes
        .iter()
        .map(|(id, set)| (*id, set.len()))
        .collect();
    bar.finish();

    println!("Generate the trip list");
    let bar = indicatif::ProgressBar::new(gtfs.trips.len() as u64);
    let mut trips: Vec<swiss_train_challenge::models::Trip> = Vec::with_capacity(gtfs.trips.len());
    for (idx, (_, trip)) in gtfs.trips.iter().enumerate() {
        bar.set_position(idx as u64);

        // Exclude non running trips on the target date
        let calendar = gtfs
            .calendar
            .get(&trip.service_id)
            .expect("calender for trip");
        let mut is_running = calendar.valid_weekday(target_date);
        match gtfs.calendar_dates.get(&trip.service_id) {
            Some(calendar_dates) => {
                for cd in calendar_dates {
                    if cd.date == target_date
                        && cd.exception_type == gtfs_structures::Exception::Deleted
                    {
                        is_running = false;
                        break;
                    }
                }
            }
            None => (),
        };
        if !is_running {
            continue;
        }

        let mut stop_times: Vec<swiss_train_challenge::models::StopTime> =
            Vec::with_capacity(trip.stop_times.len());
        let mut prev_canton: Option<CantonId> = None;
        for st in &trip.stop_times {
            match (
                st.arrival_time,
                st.departure_time,
                stop_ids.get(st.stop.id.as_str()),
                stop_cantons.get(st.stop.id.as_str()),
            ) {
                (Some(at), Some(dt), Some(stop_id), Some(canton_id)) => {
                    let num_connections = stops_connections.get(st.stop.id.as_str()).unwrap();
                    // If the stop is in the same canton as the previous one and doesn't provides
                    // connections, discard it
                    if prev_canton == Some(*canton_id) && *num_connections == 1 {
                        continue;
                    }
                    prev_canton = Some(*canton_id);

                    stop_times.push(swiss_train_challenge::models::StopTime {
                        arrival_time: at,
                        departure_time: dt,
                        stop: stops_by_id[&stop_id].clone(),
                    });
                }
                _ => (),
            }
        }
        stop_times.sort_by_key(|st| st.departure_time);

        if stop_times.len() > 1 {
            trips.push(swiss_train_challenge::models::Trip {
                id: idx as u64,
                stop_times,
            });
        }
    }
    bar.finish();
    trips.sort_by_key(|t| t.stop_times[0].departure_time);

    println!("Keep only stops used in trips");
    let used_stops: HashSet<StopId> = trips
        .iter()
        .flat_map(|t| t.stop_times.iter())
        .map(|st| st.stop.id)
        .collect();
    let used_stops: Vec<swiss_train_challenge::models::Stop> = stops
        .iter()
        .filter(|s| used_stops.contains(&s.id))
        .map(|s| swiss_train_challenge::models::Stop {
            id: s.id,
            name: s.name.clone(),
            canton: s.canton,
            transfers: s
                .transfers
                .iter()
                .filter(|t| used_stops.contains(&t.to))
                .map(|t| swiss_train_challenge::models::StopTransfer {
                    to: t.to,
                    time: t.time,
                })
                .collect(),
        })
        .collect();

    let mut message = ::capnp::message::Builder::new_default();
    let mut input_data =
        message.init_root::<swiss_train_challenge::models_capnp::input_data::Builder>();

    println!("Compute the neighboorhood between cantons");
    let mut cantons_neighbors: HashMap<CantonId, HashSet<CantonId>> = HashMap::new();
    for trip in &trips {
        let mut prev_canton: Option<CantonId> = None;
        for stoptime in &trip.stop_times {
            let canton = stops_by_id[&stoptime.stop.id].canton;
            match prev_canton {
                Some(pcanton) if pcanton != canton => match cantons_neighbors.get_mut(&pcanton) {
                    Some(neighbors) => {
                        neighbors.insert(canton);
                    }
                    None => {
                        cantons_neighbors.insert(pcanton, HashSet::from([canton]));
                    }
                },
                _ => (),
            }
            prev_canton = Some(canton);
        }
    }

    println!("Serialize stops list");
    let mut serialized_stops = input_data.reborrow().init_stops(used_stops.len() as u32);
    for (i, stop) in used_stops.iter().enumerate() {
        let mut s = serialized_stops.reborrow().get(i as u32);
        s.set_id(stop.id as u64);
        s.set_name(&stop.name);
        s.set_canton(stop.canton);
        let mut transfers = s.init_transfers(stop.transfers.len() as u32);
        for (ti, t) in stop.transfers.iter().enumerate() {
            let mut capnp_t = transfers.reborrow().get(ti as u32);
            capnp_t.set_to(t.to as u64);
            capnp_t.set_time(t.time);
        }
    }

    println!("Serialize trips list");
    let mut serialized_trips = input_data.reborrow().init_trips(trips.len() as u32);
    for (i, trip) in trips.iter().enumerate() {
        let mut t = serialized_trips.reborrow().get(i as u32);
        t.set_id(trip.id);
        let mut stop_times = t.init_stop_times(trip.stop_times.len() as u32);
        for (sti, st) in trip.stop_times.iter().enumerate() {
            let mut capnp_st = stop_times.reborrow().get(sti as u32);
            capnp_st.set_arrival_time(st.arrival_time);
            capnp_st.set_departure_time(st.departure_time);
            capnp_st.set_stop(st.stop.id as u64)
        }
    }

    println!("Serialize cantons list");
    let mut serialized_cantons = input_data.reborrow().init_cantons(cantons.0.len() as u32);
    for (i, canton) in cantons.0.iter().enumerate() {
        let mut c = serialized_cantons.reborrow().get(i as u32);
        c.set_id(canton.num);
        c.set_name(&canton.name);
    }

    println!("Serialize cantons neighbors list");
    let mut serialized_cantons_neighbors =
        input_data.init_cantons_neighbors(cantons_neighbors.len() as u32);
    for (i, (canton, neighbors)) in cantons_neighbors.iter().enumerate() {
        let mut c = serialized_cantons_neighbors.reborrow().get(i as u32);
        c.set_canton(*canton);
        let mut serialized_neighbors = c.init_neighbors(neighbors.len() as u32);
        for (j, neighbor) in neighbors.iter().enumerate() {
            serialized_neighbors.reborrow().set(j as u32, *neighbor);
        }
    }

    println!("Write serialized data to disk");
    let file = std::fs::File::create(swiss_train_challenge::INPUT_DATA_FILENAME)
        .expect("open input data file for writing");
    capnp::serialize::write_message(file, &message).expect("write capnp data to input data file");

    Ok(())
}
