use rand::seq::SliceRandom;
use rayon::prelude::*;
use std::io::Write;
use std::sync::Mutex;
use std::{collections::HashSet, sync::Arc};

use swiss_train_challenge::{
    models::{individual::Individual, CantonId},
    *,
};

const NEW_RANDOM_INDIVIDUALS_PER_GENERATION: usize = 48;
const NUMBER_OF_INDIVIDUALS_TO_KEEP: usize = 480;

const LAST_GENERATION_FILE: &str = "state.capnp.bin";
const LAST_GENERATION_BACKUP_FILE: &str = "state.backup.capnp.bin";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Read serialized data from disk");
    let context = models::Context::from_file(INPUT_DATA_FILENAME);
    println!("Read {} cantons", context.cantons.len());
    println!("Read {} stops", context.stops.len());
    println!("Read {} trips", context.trips.len());

    let mut generation: usize = 0;
    let mut individuals: Vec<Arc<Individual>> = Vec::new();
    // Collect all the orders we have tried so far
    let tried: Mutex<HashSet<Vec<CantonId>>> = Mutex::new(HashSet::new());

    // Recover previous state
    match std::fs::File::open(LAST_GENERATION_FILE) {
        Ok(file) => {
            let message_reader =
                capnp::serialize::read_message(file, capnp::message::ReaderOptions::new())
                    .expect("read capnp data");
            let input_data = message_reader
                .get_root::<crate::models_capnp::generation::Reader>()
                .expect("parse previous generation");

            let all_cantons: HashSet<CantonId> = context.cantons.iter().map(|c| c.id).collect();

            generation = input_data.get_generation() as usize;
            individuals = input_data
                .get_individuals()
                .expect("individuals")
                .iter()
                .map(|i| {
                    let visited_cantons: Vec<CantonId> = i
                        .get_visited_cantons()
                        .expect("visited cantons")
                        .iter()
                        .collect();
                    let set_of_visited_cantons: HashSet<CantonId> =
                        (&visited_cantons).iter().map(|c| *c).collect();
                    let missing_cantons = all_cantons
                        .clone()
                        .difference(&set_of_visited_cantons)
                        .map(|c| *c)
                        .collect();
                    Arc::new(Individual {
                        legs: vec![],
                        visited_cantons,
                        missing_cantons,
                        duration: i.get_duration(),
                    })
                })
                .collect();

            let mut tried_data = tried.lock().unwrap();
            for order in input_data
                .get_all()
                .expect("all orders")
                .iter()
                .map(|o| o.get_visited_cantons().unwrap().iter().collect())
            {
                tried_data.insert(order);
            }

            println!(
                "State was recovered from previous run at generation {}",
                generation
            );

            generation += 1;
        }
        _ => {
            println!("No previous state has been found, start from 0");
        }
    }

    let mut rng = rand::thread_rng();

    loop {
        println!("");
        println!("Generation {}", generation);
        println!("=============");
        println!("");
        println!(
            " {} solutions have been tried so far",
            tried.lock().unwrap().len()
        );

        println!(" Breed individuals");
        let (group_a, mut group_b) =
            swiss_train_challenge::algos::sequences::split_sorted_list_into_sorted_sublists(
                &mut rng,
                &individuals,
            );

        // This makes best individuals mate with best ones
        let group_b_bis = group_b.clone();
        let mut couples: Vec<(&&Arc<Individual>, &&Arc<Individual>)> =
            group_a.iter().zip(group_b_bis.iter()).collect();

        // Add a bit of randomness
        group_b.shuffle(&mut rng);
        let mut random_couples: Vec<(&&Arc<Individual>, &&Arc<Individual>)> =
            group_a.iter().zip(group_b.iter()).collect();

        couples.append(&mut random_couples);

        let bar = indicatif::ProgressBar::new(couples.len() as u64);
        let mut childs: Vec<Arc<Individual>> = couples
            .as_slice()
            .par_iter()
            .flat_map(|(i1, i2)| {
                bar.inc(1);
                i1.breed(&context, &tried, i2)
            })
            .collect();
        bar.finish();
        println!("    {} childs were born", childs.len());
        if individuals.len() > 20 && childs.is_empty() {
            println!("No new mutations have been created");
            break;
        }
        individuals.append(&mut childs);

        println!(" When 1 or 2 cantons are missing, bruteforce every remaining options");
        let mut bruteforced_new_individuals: Vec<Arc<Individual>> = Vec::new();
        for i in individuals.iter() {
            if i.visited_cantons.len() >= 24 {
                let mut b = i.bruteforce_one_more_canton(&context, &tried);
                bruteforced_new_individuals.append(&mut b);
            }
        }
        individuals.append(&mut bruteforced_new_individuals);


        println!(" Generate random individuals");
        let bar = indicatif::ProgressBar::new(NEW_RANDOM_INDIVIDUALS_PER_GENERATION as u64);
        let mut new_individuals: Vec<Arc<Individual>> = (0..NEW_RANDOM_INDIVIDUALS_PER_GENERATION)
            .into_par_iter()
            .flat_map(|_| {
                bar.inc(1);
                Individual::new(&context, &tried)
            })
            .collect();
        individuals.append(&mut new_individuals);
        bar.finish();

        individuals.sort_unstable_by_key(|i| i.score());

        // Keep only the bests
        individuals.truncate(NUMBER_OF_INDIVIDUALS_TO_KEEP);

        // Serialize generation
        let mut message = ::capnp::message::Builder::new_default();
        let mut s_generation =
            message.init_root::<swiss_train_challenge::models_capnp::generation::Builder>();
        s_generation.set_generation(generation as u32);
        let mut serialized_individuals = s_generation
            .reborrow()
            .init_individuals(individuals.len() as u32);
        for (i, individu) in individuals.iter().enumerate() {
            let mut s = serialized_individuals.reborrow().get(i as u32);
            s.set_duration(individu.duration);
            let mut s_cantons = s.init_visited_cantons(individu.visited_cantons.len() as u32);
            for (j, canton) in individu.visited_cantons.iter().enumerate() {
                s_cantons.reborrow().set(j as u32, *canton);
            }
        }
        let tried_data = tried.lock().unwrap();
        let mut serialized_all = s_generation.init_all(tried_data.len() as u32);
        for (i, order) in tried_data.iter().enumerate() {
            let s = serialized_all.reborrow().get(i as u32);
            let mut s_cantons = s.init_visited_cantons(order.len() as u32);
            for (j, c) in order.iter().enumerate() {
                s_cantons.reborrow().set(j as u32, *c);
            }
        }

        if std::path::Path::new(LAST_GENERATION_FILE).exists() {
            std::fs::rename(LAST_GENERATION_FILE, LAST_GENERATION_BACKUP_FILE)
                .expect("backup last state");
        }
        let file =
            std::fs::File::create(LAST_GENERATION_FILE).expect("open genration file for writing");
        capnp::serialize::write_message(file, &message)
            .expect("write capnp data to genration file");

        let mut textual_backup = std::fs::File::create(format!("gen-{}.txt", generation))
            .expect("open generation file for writing");
        println!(" Best individuals:");
        for (idx, i) in individuals.iter().enumerate() {
            let named = i.with_names(&context);
            if idx < 8 {
                println!(
                    "  - visit {} cantons in {}\n      cantons: {}\n      missing: {}",
                    named.visited_cantons.len(),
                    named.duration,
                    named.visited_cantons.join(", "),
                    named.missing_cantons.join(", ")
                );
            }
            write!(textual_backup, "{}\n", named).expect("write individual");
        }

        let mut n26 = 0;
        let mut n25 = 0;
        let mut n24 = 0;
        let mut n23 = 0;
        let mut n22 = 0;
        let mut n21 = 0;
        for i in individuals.iter() {
            let l = i.visited_cantons.len();
            if l == 26 {
                n26 += 1;
            }
            if l == 25 {
                n25 += 1;
            }
            if l == 24 {
                n24 += 1;
            }
            if l == 23 {
                n23 += 1;
            }
            if l == 22 {
                n22 += 1;
            }
            if l == 21 {
                n21 += 1;
            }
        }
        println!("  {} solutions visit 26 cantons", n26);
        println!("  {} solutions visit 25 cantons", n25);
        println!("  {} solutions visit 24 cantons", n24);
        println!("  {} solutions visit 23 cantons", n23);
        println!("  {} solutions visit 22 cantons", n22);
        println!("  {} solutions visit 21 cantons", n21);

        generation += 1;
    }

    Ok(())
}
