use geo::{LineString, Point, Polygon};
use quick_xml::events::Event;

use crate::preprocess::cantons::{Canton, Cantons};

const CANTON_ELEM_NAME: &[u8; 62] =
    b"swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET";

/// Parse a XTF file content into a list of Canton, whose boundary is using WGS84 for coordinates
/// representation
pub fn parse_xtf(content: &str) -> Result<Cantons, String> {
    let from = "LV95";
    let to = "WGS84";
    let projection =
        geo::transform::Proj::new_known_crs(&from, &to, None).expect("failed to create projection");

    let mut cantons: Vec<Canton> = Vec::new();

    let mut reader = quick_xml::Reader::from_str(content);

    loop {
        match reader.read_event() {
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) if e.name().as_ref() == CANTON_ELEM_NAME => {
                let mut name: Option<String> = None;
                let mut num: Option<u64> = None;
                let mut coords: Vec<Point<f64>> = Vec::new();
                let mut is_canton: bool = false;

                loop {
                    match reader.read_event() {
                        Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                        Ok(Event::End(e)) if e.name().as_ref() == CANTON_ELEM_NAME => break,
                        Ok(Event::Start(e)) if e.name().as_ref() == b"Name" => {
                            let Event::Text(value) = reader.read_event().unwrap() else {
                                panic!("expect a text")
                            };
                            name = Some(value.unescape().unwrap().into_owned());
                        }
                        Ok(Event::Start(e)) if e.name().as_ref() == b"Kantonsnummer" => {
                            let Event::Text(value) = reader.read_event().unwrap() else {
                                panic!("expect a text")
                            };
                            num = Some(value.unescape().unwrap().parse().unwrap());
                        }
                        Ok(Event::Start(e)) if e.name().as_ref() == b"Objektart" => {
                            let Event::Text(value) = reader.read_event().unwrap() else {
                                panic!("expect a text")
                            };
                            is_canton = value.unescape().unwrap() == "Kanton";
                        }
                        Ok(Event::Start(e)) if e.name().as_ref() == b"MultiSurface" => loop {
                            match reader.read_event() {
                                Err(e) => panic!(
                                    "Error at position {}: {:?}",
                                    reader.buffer_position(),
                                    e
                                ),
                                Ok(Event::Start(e)) if e.name().as_ref() == b"COORD" => {
                                    let mut lat: Option<f64> = None;
                                    let mut lon: Option<f64> = None;
                                    loop {
                                        match reader.read_event().unwrap() {
                                            Event::Start(e) if e.name().as_ref() == b"C1" => {
                                                let Event::Text(value) =
                                                    reader.read_event().unwrap()
                                                else {
                                                    panic!("expect a text")
                                                };
                                                lat = Some(
                                                    value.unescape().unwrap().parse().unwrap(),
                                                );
                                            }
                                            Event::Start(e) if e.name().as_ref() == b"C2" => {
                                                let Event::Text(value) =
                                                    reader.read_event().unwrap()
                                                else {
                                                    panic!("expect a text")
                                                };
                                                lon = Some(
                                                    value.unescape().unwrap().parse().unwrap(),
                                                );
                                            }
                                            Event::End(e) if e.name().as_ref() == b"COORD" => break,
                                            _ => (),
                                        }
                                    }
                                    let point = projection
                                        .convert(Point::new(lat.unwrap(), lon.unwrap()))
                                        .unwrap();
                                    coords.push(point);
                                }
                                Ok(Event::End(e)) if e.name().as_ref() == b"MultiSurface" => break,
                                _ => (),
                            }
                        },
                        _ => (),
                    }
                }

                if is_canton {
                    cantons.push(Canton {
                        name: name.unwrap(),
                        num: num.unwrap(),
                        border: Polygon::new(LineString::from(coords), vec![]),
                    })
                }
            }
            _ => (),
        }
    }

    Ok(Cantons(cantons))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_xtf_file() {
        const CONTENT: &str = r#"
<?xml version="1.0" encoding="UTF-8"?>
<TRANSFER xmlns="http://www.interlis.ch/INTERLIS2.3">
  <HEADERSECTION SENDER="ili2fme-7.2.1-aa3c48c318db91416e944ac8d2fb0142426ab234" VERSION="2.3">
    <MODELS>
      <MODEL NAME="CoordSys" VERSION="2015-11-24" URI="http://www.interlis.ch/models"></MODEL>
      <MODEL NAME="Units" VERSION="2012-02-20" URI="http://www.interlis.ch/models"></MODEL>
      <MODEL NAME="GeometryCHLV03_V1" VERSION="2017-12-04" URI="http://www.geo.admin.ch"></MODEL>
      <MODEL NAME="GeometryCHLV95_V1" VERSION="2017-12-04" URI="http://www.geo.admin.ch"></MODEL>
      <MODEL NAME="swissBOUNDARIES3D_ili2_LV95_V1_5" VERSION="2024-01-01" URI="https://models.geo.admin.ch/Swisstopo"></MODEL>
    </MODELS>
  </HEADERSECTION>
  <DATASECTION>
    <swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN BID="2">
      <swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET TID="921DFEF2-6D91-4CB8-9CFC-2A831C412020">
        <Objektart>Kanton</Objektart>
        <Kantonsnummer>22</Kantonsnummer>
        <Name>Vaud</Name>
        <MultiSurface>
          <swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
            <Surfaces>
              <swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
                <Surface>
                  <SURFACE>
                    <BOUNDARY>
                      <POLYLINE>
                        <COORD><C1>2557141.997</C1><C2>1134021.156</C2><C3>373.596</C3></COORD>
                        <COORD><C1>2557151.305</C1><C2>1134063.637</C2><C3>373.538</C3></COORD>
                      </POLYLINE>
                    </BOUNDARY>
                  </SURFACE>
                </Surface>
              </swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
            </Surfaces>
          </swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
        </MultiSurface>
      </swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET>
      <swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET TID="921DFEF2-6D91-4CB8-9CFC-2A831C412020">
        <Objektart>Kanton</Objektart>
        <Kantonsnummer>23</Kantonsnummer>
        <Name>Other</Name>
        <MultiSurface>
          <swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
            <Surfaces>
              <swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
                <Surface>
                  <SURFACE>
                    <BOUNDARY>
                      <POLYLINE>
                        <COORD><C1>2557141.997</C1><C2>1134021.156</C2><C3>373.596</C3></COORD>
                      </POLYLINE>
                    </BOUNDARY>
                  </SURFACE>
                </Surface>
              </swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
            </Surfaces>
          </swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
        </MultiSurface>
      </swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET>
      <swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET TID="921DFEF2-6D91-4CB8-9CFC-2A831C412020">
        <Objektart>Fake</Objektart>
        <Kantonsnummer>24</Kantonsnummer>
        <Name>Fake</Name>
        <MultiSurface>
          <swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
            <Surfaces>
              <swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
                <Surface>
                  <SURFACE>
                    <BOUNDARY>
                      <POLYLINE>
                        <COORD><C1>2557141.997</C1><C2>1134021.156</C2><C3>373.596</C3></COORD>
                      </POLYLINE>
                    </BOUNDARY>
                  </SURFACE>
                </Surface>
              </swissBOUNDARIES3D_ili2_LV95_V1_5.Surface3d>
            </Surfaces>
          </swissBOUNDARIES3D_ili2_LV95_V1_5.MultiSurface3d>
        </MultiSurface>
      </swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN.TLM_KANTONSGEBIET>
    </swissBOUNDARIES3D_ili2_LV95_V1_5.TLM_GRENZEN>
  </DATASECTION>
</TRANSFER>
        "#;

        let parsed = parse_xtf(CONTENT).expect("parse xml");

        let expected = Cantons(vec![
            Canton {
                name: "Vaud".to_owned(),
                num: 22,
                border: Polygon::new(
                    LineString::from(vec![
                        Point::new(6.8818198948945755, 46.35619778084918),
                        Point::new(6.881936897246594, 46.35658051004114),
                    ]),
                    vec![],
                ),
            },
            Canton {
                name: "Other".to_owned(),
                num: 23,
                border: Polygon::new(
                    LineString::from(vec![Point::new(6.8818198948945755, 46.35619778084918)]),
                    vec![],
                ),
            },
        ]);

        assert_eq!(parsed, expected);
    }
}
