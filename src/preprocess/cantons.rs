use geo::{Contains, Point, Polygon};

#[derive(Debug, PartialEq)]
pub struct Canton {
    pub name: String,
    pub num: u64,
    pub border: Polygon<f64>,
}

#[derive(PartialEq, Debug)]
pub struct Cantons(pub Vec<Canton>);

impl Cantons {
    pub fn get_canton_of(&self, stop: &gtfs_structures::Stop) -> Option<&Canton> {
        let point = Point::new(
            stop.longitude.expect("longitude"),
            stop.latitude.expect("latitude"),
        );
        self.0.iter().find(|c| c.border.contains(&point))
    }
}
