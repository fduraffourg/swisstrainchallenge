use ringbuffer::{AllocRingBuffer, RingBuffer};

use crate::models::{CantonId, Leg, Route, Stop, TripAtStop};
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    rc::Rc,
    sync::Arc,
};

use crate::models::StopId;

use super::sequences::common_prefix_size;

// Cache previous route computation states
thread_local! {static CACHE: RefCell<AllocRingBuffer<RouteCollector>> = RefCell::new(AllocRingBuffer::new(16));}

/// This is a linked list to collect legs without
/// having to clone them while the route increases
#[derive(Debug, PartialEq)]
pub enum LegLL {
    Nil,
    Item { leg: Rc<Leg>, prev: Rc<LegLL> },
}

impl LegLL {
    pub fn iter(&self) -> LegLLIterator {
        match self {
            LegLL::Nil => LegLLIterator {
                cur: Rc::new(LegLL::Nil),
            },
            LegLL::Item { leg, prev } => LegLLIterator {
                cur: Rc::new(LegLL::Item {
                    leg: leg.clone(),
                    prev: prev.clone(),
                }),
            },
        }
    }
}

impl IntoIterator for LegLL {
    type Item = Rc<Leg>;
    type IntoIter = LegLLIterator;
    fn into_iter(self) -> Self::IntoIter {
        LegLLIterator { cur: Rc::new(self) }
    }
}

pub struct LegLLIterator {
    cur: Rc<LegLL>,
}

impl Iterator for LegLLIterator {
    type Item = Rc<Leg>;
    fn next(&mut self) -> Option<Self::Item> {
        match &*self.cur.clone() {
            LegLL::Nil => None,
            LegLL::Item { leg, prev } => {
                self.cur = prev.clone();
                Some(leg.clone())
            }
        }
    }
}

/// Internal structure used to compute a route based on the dijkstra algo
struct DRoute {
    pos: usize,
    dst: StopId,
    arrival_time: u32,
    legs: Rc<LegLL>,
}

/// Structure that collects best routes to nodes of the graph
pub struct RouteCollector {
    pub cantons: Vec<CantonId>,
    routes: HashMap<(usize, StopId), Rc<DRoute>>,
    stops_with_new_routes: HashSet<(usize, StopId)>,
    best_route: DRoute,
}

impl RouteCollector {
    fn new(stops: &Vec<Arc<Stop>>, cantons: Vec<CantonId>) -> Self {
        // Try to find a RouteCollector and use it
        CACHE.with_borrow(|collectors| {
            let mut best_collector: Option<&RouteCollector> = None;
            let mut best_match_size = 0;
            for collector in collectors {
                let match_size = common_prefix_size(&collector.cantons, &cantons);
                if match_size > best_match_size {
                    best_match_size = match_size;
                    best_collector = Some(&collector);
                }
            }

            match best_collector {
                Some(collector) => RouteCollector::from(collector, cantons),
                None => {
                    let first_canton = cantons[0];
                    let mut droutes = RouteCollector {
                        cantons,
                        routes: HashMap::new(),
                        stops_with_new_routes: HashSet::new(),
                        best_route: DRoute {
                            pos: 0,
                            dst: 0,
                            arrival_time: u32::MAX,
                            legs: Rc::new(LegLL::Nil),
                        },
                    };
                    for stop in stops {
                        if stop.canton == first_canton {
                            droutes.collect_stop(0, stop, 0, || Rc::new(LegLL::Nil));
                        }
                    }
                    droutes
                }
            }
        })
    }

    fn from(other: &RouteCollector, cantons: Vec<CantonId>) -> Self {
        let diverge_at = common_prefix_size(&other.cantons, &cantons);

        let routes = other
            .routes
            .iter()
            .filter(|(&(pos, _), _)| pos < diverge_at - 1)
            .map(|(k, v)| (*k, v.clone()))
            .collect();
        let stops_with_new_routes = other
            .routes
            .iter()
            .filter(|(&(pos, _), _)| pos == diverge_at - 2)
            .map(|(key, _)| *key)
            .collect();
        let best_route = DRoute {
            pos: 0,
            dst: 0,
            arrival_time: u32::MAX,
            legs: Rc::new(LegLL::Nil),
        };

        RouteCollector {
            cantons,
            routes,
            stops_with_new_routes,
            best_route,
        }
    }

    fn save_in_cache(self) {
        CACHE.with_borrow_mut(|c| c.push(self));
    }

    fn collect_stop<F>(&mut self, prev_pos: usize, dst: &Stop, arrival_time: u32, legs_cons: F)
    where
        F: Fn() -> Rc<LegLL>,
    {
        // If we reached the last canton, either it's a best route, either we can ignore it
        if prev_pos == self.cantons.len() - 2 && dst.canton == self.cantons[self.cantons.len() - 1]
        {
            // Update the best route if we found a new best
            if arrival_time < self.best_route.arrival_time {
                self.best_route.dst = dst.id;
                self.best_route.arrival_time = arrival_time;
                self.best_route.legs = legs_cons();
            }
        } else {
            let mut fn_collect = |pos| match self.routes.get(&(pos, dst.id)) {
                Some(known_route) if known_route.arrival_time <= arrival_time => (),
                _ => {
                    let legs = legs_cons();
                    let new_route = Rc::new(DRoute {
                        pos,
                        dst: dst.id,
                        arrival_time,
                        legs: legs.clone(),
                    });
                    self.routes.insert((pos, dst.id), new_route);
                    self.stops_with_new_routes.insert((pos, dst.id));

                    for stop_transfer in &dst.transfers {
                        match self.routes.get(&(pos, stop_transfer.to)) {
                            Some(known_route) if known_route.arrival_time <= arrival_time => (),
                            _ => {
                                let transfer_arrival_time = arrival_time + stop_transfer.time;
                                let transfer_route = Rc::new(DRoute {
                                    pos,
                                    dst: stop_transfer.to,
                                    arrival_time: transfer_arrival_time,
                                    legs: legs.clone(),
                                });
                                self.routes.insert((pos, stop_transfer.to), transfer_route);
                                self.stops_with_new_routes.insert((pos, stop_transfer.to));
                            }
                        }
                    }
                }
            };

            if self.cantons[0..prev_pos + 1].contains(&dst.canton) {
                fn_collect(prev_pos);
            } else if self.cantons[prev_pos + 1] == dst.canton {
                fn_collect(prev_pos + 1);
            }
        }
    }

    fn get_best_route(&self) -> Option<Route> {
        if self.best_route.arrival_time == u32::MAX {
            None
        } else {
            let mut legs: Vec<Arc<Leg>> = self
                .best_route
                .legs
                .iter()
                .map(|l| {
                    Arc::new(Leg {
                        from: l.from,
                        to: l.to,
                        arrival_time: l.arrival_time,
                        trip: l.trip,
                    })
                })
                .collect();
            legs.reverse();
            Some(Route {
                dst: self.best_route.dst,
                arrival_time: self.best_route.arrival_time,
                legs,
            })
        }
    }

    fn best_arrival_time(&self) -> u32 {
        self.best_route.arrival_time
    }

    fn next(&mut self) -> Option<Rc<DRoute>> {
        let next_stop_id = self
            .stops_with_new_routes
            .iter()
            .next()
            .map(|id| id.clone());
        match next_stop_id {
            Some(next_stop_id) => {
                self.stops_with_new_routes.remove(&next_stop_id);
                Some(self.routes.get(&next_stop_id).unwrap().clone())
            }
            None => None,
        }
    }
}

/// Run the dijkstra algorithm on the given schedule and let a RouteCollector filter the best route
pub fn find_best_route_through_cantons(
    trips_at_stop: &HashMap<StopId, Vec<TripAtStop>>,
    stops: &Vec<Arc<Stop>>,
    cantons: &Vec<CantonId>,
) -> Option<Route> {
    assert!(cantons.len() > 1);

    let mut collector = RouteCollector::new(stops, cantons.clone());

    while let Some(route) = collector.next() {
        let trips_for_stop = trips_at_stop.get(&route.dst);
        let best_arrival_time = collector.best_arrival_time();
        let trips = trips_for_stop
            .iter()
            .flat_map(|trips| trips.iter())
            .filter(|t| {
                t.departure_time > route.arrival_time && t.departure_time < best_arrival_time
            });
        let allowed_cantons: &[CantonId] = &cantons[0..route.pos + 2];

        for trip in trips {
            let stoptimes = trip
                .trip
                .stop_times
                .iter()
                // Consider only stops after the current one
                .skip_while(|st| {
                    !(st.stop.id == route.dst && st.arrival_time >= route.arrival_time)
                })
                .skip(1);
            for stoptime in stoptimes {
                // Stop riding the trip as soon as it reaches unallowed cantons
                let mut allowed = false;
                for j in (0..allowed_cantons.len()).rev() {
                    if stoptime.stop.canton == allowed_cantons[j] {
                        allowed = true;
                        break;
                    }
                }
                if !allowed {
                    break;
                }

                collector.collect_stop(route.pos, &stoptime.stop, stoptime.arrival_time, || {
                    Rc::new(LegLL::Item {
                        leg: Rc::new(Leg {
                            from: route.dst,
                            to: stoptime.stop.id,
                            arrival_time: stoptime.arrival_time,
                            trip: trip.trip.id,
                        }),
                        prev: route.legs.clone(),
                    })
                });
            }
        }
    }

    let result = collector.get_best_route();
    collector.save_in_cache();
    result
}

#[cfg(test)]
mod tests {
    use crate::models::{trips_at_stop, StopTime, Trip};

    use super::*;

    #[test]
    fn test_find_route_through_cantons_1() {
        let lausanne = Arc::new(Stop {
            id: 1,
            name: "lausanne".to_owned(),
            canton: 1,
            transfers: vec![],
        });
        let bern = Arc::new(Stop {
            id: 2,
            name: "bern".to_owned(),
            canton: 2,
            transfers: vec![],
        });
        let bellinzona = Arc::new(Stop {
            id: 3,
            name: "bellinzona".to_owned(),
            canton: 3,
            transfers: vec![],
        });
        let grono = Arc::new(Stop {
            id: 4,
            name: "grono".to_owned(),
            canton: 4,
            transfers: vec![],
        });
        let olten = Arc::new(Stop {
            id: 5,
            name: "olten".to_owned(),
            canton: 5,
            transfers: vec![],
        });
        let stops: Vec<Arc<Stop>> = vec![
            lausanne.clone(),
            bern.clone(),
            bellinzona.clone(),
            grono.clone(),
            olten.clone(),
        ];

        let t1 = Arc::new(Trip {
            id: 1,
            stop_times: vec![
                StopTime {
                    arrival_time: 0,
                    departure_time: 1,
                    stop: lausanne.clone(),
                },
                StopTime {
                    arrival_time: 60,
                    departure_time: 62,
                    stop: bern.clone(),
                },
            ],
        });
        let t2 = Arc::new(Trip {
            id: 2,
            stop_times: vec![
                StopTime {
                    arrival_time: 60,
                    departure_time: 62,
                    stop: bern.clone(),
                },
                StopTime {
                    arrival_time: 150,
                    departure_time: 152,
                    stop: bellinzona.clone(),
                },
            ],
        });

        let t3 = Arc::new(Trip {
            id: 3,
            stop_times: vec![
                StopTime {
                    arrival_time: 150,
                    departure_time: 152,
                    stop: bellinzona.clone(),
                },
                StopTime {
                    arrival_time: 180,
                    departure_time: 181,
                    stop: grono.clone(),
                },
            ],
        });
        let t4 = Arc::new(Trip {
            id: 4,
            stop_times: vec![
                StopTime {
                    arrival_time: 185,
                    departure_time: 186,
                    stop: grono.clone(),
                },
                StopTime {
                    arrival_time: 216,
                    departure_time: 217,
                    stop: bellinzona.clone(),
                },
            ],
        });
        let t5 = Arc::new(Trip {
            id: 5,
            stop_times: vec![
                StopTime {
                    arrival_time: 216,
                    departure_time: 218,
                    stop: bellinzona.clone(),
                },
                StopTime {
                    arrival_time: 300,
                    departure_time: 302,
                    stop: bern.clone(),
                },
            ],
        });
        let t6 = Arc::new(Trip {
            id: 6,
            stop_times: vec![
                StopTime {
                    arrival_time: 300,
                    departure_time: 305,
                    stop: bern.clone(),
                },
                StopTime {
                    arrival_time: 330,
                    departure_time: 332,
                    stop: olten.clone(),
                },
            ],
        });
        let t7 = Arc::new(Trip {
            id: 7,
            stop_times: vec![
                StopTime {
                    arrival_time: 10,
                    departure_time: 12,
                    stop: lausanne.clone(),
                },
                StopTime {
                    arrival_time: 100,
                    departure_time: 102,
                    stop: olten.clone(),
                },
            ],
        });

        let trips = vec![t1, t2, t3, t4, t5, t6, t7];
        let tas = trips_at_stop(&trips);
        let cantons = vec![1, 2, 3, 4, 5];

        let route = find_best_route_through_cantons(&tas, &stops, &cantons).unwrap();
        let expected_route = Route {
            dst: olten.id,
            arrival_time: 330,
            legs: vec![
                Arc::new(Leg {
                    from: lausanne.id,
                    to: bern.id,
                    arrival_time: 60,
                    trip: 1,
                }),
                Arc::new(Leg {
                    from: bern.id,
                    to: bellinzona.id,
                    arrival_time: 150,
                    trip: 2,
                }),
                Arc::new(Leg {
                    from: bellinzona.id,
                    to: grono.id,
                    arrival_time: 180,
                    trip: 3,
                }),
                Arc::new(Leg {
                    from: grono.id,
                    to: bellinzona.id,
                    arrival_time: 216,
                    trip: 4,
                }),
                Arc::new(Leg {
                    from: bellinzona.id,
                    to: bern.id,
                    arrival_time: 300,
                    trip: 5,
                }),
                Arc::new(Leg {
                    from: bern.id,
                    to: olten.id,
                    arrival_time: 330,
                    trip: 6,
                }),
            ],
        };
        assert_eq!(route, expected_route);

        let route2 = find_best_route_through_cantons(&tas, &stops, &cantons).unwrap();
        assert_eq!(route2, expected_route);
    }

    #[test]
    fn test_find_route_through_cantons_2() {
        let lausanne = Arc::new(Stop {
            id: 1,
            name: "lausanne".to_owned(),
            canton: 1,
            transfers: vec![],
        });
        let bern = Arc::new(Stop {
            id: 2,
            name: "bern".to_owned(),
            canton: 2,
            transfers: vec![],
        });
        let olten = Arc::new(Stop {
            id: 3,
            name: "olten".to_owned(),
            canton: 3,
            transfers: vec![],
        });
        let zurich = Arc::new(Stop {
            id: 4,
            name: "zurich".to_owned(),
            canton: 4,
            transfers: vec![],
        });
        let stops: Vec<Arc<Stop>> = vec![
            lausanne.clone(),
            bern.clone(),
            olten.clone(),
            zurich.clone(),
        ];

        let t1 = Arc::new(Trip {
            id: 1,
            stop_times: vec![
                StopTime {
                    arrival_time: 0,
                    departure_time: 1,
                    stop: lausanne.clone(),
                },
                StopTime {
                    arrival_time: 60,
                    departure_time: 62,
                    stop: bern.clone(),
                },
                StopTime {
                    arrival_time: 120,
                    departure_time: 122,
                    stop: olten.clone(),
                },
                StopTime {
                    arrival_time: 180,
                    departure_time: 182,
                    stop: zurich.clone(),
                },
            ],
        });

        let trips = vec![t1];
        let tas = trips_at_stop(&trips);
        let cantons = vec![1, 4];

        let result = find_best_route_through_cantons(&tas, &stops, &cantons);
        assert!(result.is_none());
    }

    // Ensure the algo can follow a trip that goes along cantons
    #[test]
    fn test_find_route_through_cantons_3() {
        let lausanne = Arc::new(Stop {
            id: 1,
            name: "lausanne".to_owned(),
            canton: 1,
            transfers: vec![],
        });
        let bern = Arc::new(Stop {
            id: 2,
            name: "bern".to_owned(),
            canton: 2,
            transfers: vec![],
        });
        let olten = Arc::new(Stop {
            id: 3,
            name: "olten".to_owned(),
            canton: 3,
            transfers: vec![],
        });
        let zurich = Arc::new(Stop {
            id: 4,
            name: "zurich".to_owned(),
            canton: 4,
            transfers: vec![],
        });
        let stops: Vec<Arc<Stop>> = vec![
            lausanne.clone(),
            bern.clone(),
            olten.clone(),
            zurich.clone(),
        ];

        let t1 = Arc::new(Trip {
            id: 1,
            stop_times: vec![
                StopTime {
                    arrival_time: 0,
                    departure_time: 1,
                    stop: lausanne.clone(),
                },
                StopTime {
                    arrival_time: 60,
                    departure_time: 62,
                    stop: bern.clone(),
                },
                StopTime {
                    arrival_time: 120,
                    departure_time: 122,
                    stop: olten.clone(),
                },
                StopTime {
                    arrival_time: 180,
                    departure_time: 182,
                    stop: zurich.clone(),
                },
            ],
        });

        let trips = vec![t1.clone()];
        let tas = trips_at_stop(&trips);
        let cantons = vec![1, 2, 3, 4];

        let route = find_best_route_through_cantons(&tas, &stops, &cantons).expect("route");
        assert_eq!(route.legs.len(), 3);
        for leg in route.legs {
            assert_eq!(leg.trip, t1.id);
        }
    }
}
