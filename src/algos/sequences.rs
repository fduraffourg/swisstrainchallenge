use rand::{
    rngs::ThreadRng,
    seq::{IteratorRandom, SliceRandom},
};

/// Choose randomly a possible ways to insert the sequence containing the element `elem` from
/// `other` into `into` and do it.
pub fn insert_seq<T>(mut rng: &mut ThreadRng, into: &[T], other: &[T], elem: T) -> Option<Vec<T>>
where
    T: PartialEq + Copy,
{
    match other.iter().enumerate().find(|(_, e)| **e == elem) {
        None => None,
        Some((pos_in_other, _)) => {
            let starts: Vec<(usize, usize)> = other[0..pos_in_other]
                .iter()
                .enumerate()
                .flat_map(|(op, oe)| {
                    into.iter()
                        .enumerate()
                        .find(|(_, ie)| *ie == oe)
                        .map(|(pos, _)| (pos, op))
                })
                .collect();
            let mut ends: Vec<(usize, usize)> = other[pos_in_other + 1..]
                .iter()
                .enumerate()
                .flat_map(|(op, oe)| {
                    into.iter()
                        .enumerate()
                        .find(|(_, ie)| *ie == oe)
                        .map(|(pos, _)| (pos, op + pos_in_other + 1))
                })
                .collect();
            ends.push((into.len(), other.len()));

            match starts
                .iter()
                .flat_map(|start| ends.iter().map(|end| (start.clone(), end)))
                .filter(|((istart, _), (iend, _))| istart < iend)
                .choose(&mut rng)
            {
                None => None,
                Some(((sstart, ostart), &(send, oend))) => Some(
                    into[0..sstart]
                        .iter()
                        .chain(other[ostart..oend].iter())
                        .chain(into[send..].iter())
                        .map(|c| *c)
                        .collect(),
                ),
            }
        }
    }
}

pub fn remove_duplicates<T>(mut input: Vec<T>) -> Vec<T>
where
    T: PartialEq,
{
    let mut positions_to_remove: Vec<usize> = Vec::new();
    for (pos, elem) in input.iter().enumerate().rev() {
        if input[0..pos].contains(elem) {
            positions_to_remove.push(pos);
        }
    }

    for pos in positions_to_remove {
        input.remove(pos);
    }

    input
}

/// Randomly subdivided the given sublist into 2 sorted sublists of equal size
pub fn split_sorted_list_into_sorted_sublists<'a, T>(
    mut rng: &mut ThreadRng,
    input: &'a Vec<T>,
) -> (Vec<&'a T>, Vec<&'a T>) {
    let half = input.len() / 2;
    let mut attribution_map: Vec<usize> = std::iter::repeat(0)
        .take(half)
        .chain(std::iter::repeat(1).take(half))
        .collect();
    attribution_map.shuffle(&mut rng);

    let mut a: Vec<&T> = Vec::with_capacity(half);
    let mut b: Vec<&T> = Vec::with_capacity(half);
    for (&attribution, elem) in attribution_map.iter().zip(input.iter()) {
        if attribution == 0 {
            a.push(elem);
        } else {
            b.push(elem);
        }
    }
    (a, b)
}

pub fn common_prefix_size<T>(a: &[T], b: &[T]) -> usize
where
    T: PartialEq,
{
    let mut i = 0;
    for (ia, ib) in a.iter().zip(b.iter()) {
        if ia == ib {
            i += 1;
        } else {
            break;
        }
    }
    i
}

// Generate all possible options of inserting a new element in a sequence
pub fn insertion_candidates<T>(seq: &[T], element: T) -> Vec<Vec<T>>
where
    T: Clone,
{
    let len = seq.len() + 1;
    let mut result: Vec<Vec<T>> = Vec::with_capacity(len);

    for pos in 0..len {
        let mut vec: Vec<T> = Vec::with_capacity(len);
        for e in &seq[0..pos] {
            vec.push(e.clone());
        }
        vec.push(element.clone());
        for e in &seq[pos..] {
            vec.push(e.clone());
        }
        result.push(vec);
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_insert_seq_into_1() {
        let mut rng = rand::thread_rng();
        let into = vec![1, 2, 3, 4, 5];
        let other = vec![12, 13, 14, 3, 6, 7, 8, 5];
        let res = insert_seq(&mut rng, &into, &other, 7);
        assert_eq!(res, Some(vec![1, 2, 3, 6, 7, 8, 5]));
    }

    #[test]
    fn test_insert_seq_into_2() {
        let mut rng = rand::thread_rng();
        let into = vec![1, 2, 3, 4, 5, 22];
        let other = vec![12, 1, 42, 3, 6, 7, 8, 5];
        let res = insert_seq(&mut rng, &into, &other, 7).expect("a solution");
        let solutions = vec![
            vec![1, 2, 3, 6, 7, 8, 5],
            vec![1, 42, 3, 6, 7, 8, 5],
            vec![1, 2, 3, 6, 7, 8, 5, 22],
            vec![1, 42, 3, 6, 7, 8, 5, 22],
        ];
        assert!(solutions.contains(&res));
    }

    #[test]
    fn test_remove_duplicates() {
        let input = vec![1, 2, 3, 4, 5, 1, 2, 3];
        assert_eq!(remove_duplicates(input), vec![1, 2, 3, 4, 5]);

        let input = vec![1, 1, 1, 1, 2, 2, 2, 1, 2, 1, 2];
        assert_eq!(remove_duplicates(input), vec![1, 2]);
    }

    #[test]
    fn test_split_sorted_list_into_sorted_sublists() {
        let mut rng = rand::thread_rng();
        for i in 20..100 {
            let input: Vec<usize> = (0..i).collect();
            let (a, b) = split_sorted_list_into_sorted_sublists(&mut rng, &input);
            assert_eq!(a.len(), b.len());
            assert!([input.len(), input.len() - 1].contains(&(a.len() + b.len())));
            // Check that a is sorted
            for (i, j) in a.iter().zip(a.iter().skip(1)) {
                assert!(i < j);
            }
            // Check that b is sorted
            for (i, j) in b.iter().zip(b.iter().skip(1)) {
                assert!(i < j);
            }
        }
    }

    #[test]
    fn test_common_prefix_size() {
        let a = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        let b = [1, 2, 3, 4, 5, 7, 8, 9];
        assert_eq!(common_prefix_size(&a, &a), 9);
        assert_eq!(common_prefix_size(&a, &b), 5);
        assert_eq!(common_prefix_size(&a, &[0]), 0);
        assert_eq!(common_prefix_size(&a, &[]), 0);
    }

    #[test]
    fn test_insertion_candidates() {
        let a = [1, 2, 3, 4, 5];
        let res = insertion_candidates(&a, 0);
        let expected = vec![
            vec![0, 1, 2, 3, 4, 5],
            vec![1, 0, 2, 3, 4, 5],
            vec![1, 2, 0, 3, 4, 5],
            vec![1, 2, 3, 0, 4, 5],
            vec![1, 2, 3, 4, 0, 5],
            vec![1, 2, 3, 4, 5, 0],
        ];
        assert_eq!(res, expected);
    }
}
