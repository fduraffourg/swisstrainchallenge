use std::{
    collections::HashSet,
    sync::{Arc, Mutex},
};

use rand::seq::{IteratorRandom, SliceRandom};

use super::{CantonId, Context, Leg};

pub struct Individual {
    pub legs: Vec<Arc<Leg>>,
    pub visited_cantons: Vec<CantonId>,
    pub missing_cantons: HashSet<CantonId>,
    pub duration: u32,
}

impl Individual {
    pub fn new(
        ctx: &Context,
        tried_solutions: &Mutex<HashSet<Vec<CantonId>>>,
    ) -> Option<Arc<Self>> {
        let mut rng = rand::thread_rng();

        let first_canton: CantonId = ctx.cantons.iter().choose(&mut rng).expect("canton").id;
        let mut cantons: Vec<CantonId> = Vec::from([first_canton]);
        let mut last_canton: CantonId = first_canton;
        loop {
            let neighbors = &ctx.cantons_neighbors[&last_canton];
            let mut unvisited_neighbors: Vec<&CantonId> =
                neighbors.iter().filter(|n| !cantons.contains(n)).collect();
            if unvisited_neighbors.is_empty() {
                if cantons.len() > 2 {
                    let neighbors = &ctx.cantons_neighbors[&cantons[cantons.len() - 2]];
                    for n in neighbors.iter().filter(|n| !cantons.contains(n)) {
                        unvisited_neighbors.push(n);
                    }
                }
                break;
            }
            let next_canton: CantonId = **unvisited_neighbors
                .iter()
                .choose(&mut rng)
                .expect("random canton");
            cantons.push(next_canton);
            last_canton = next_canton
        }

        Individual::generate_from_cantons_order(ctx, tried_solutions, cantons)
    }

    fn generate_from_cantons_order(
        ctx: &Context,
        tried_solutions: &Mutex<HashSet<Vec<CantonId>>>,
        cantons: Vec<CantonId>,
    ) -> Option<Arc<Self>> {
        let is_tried = {
            let data = tried_solutions.lock().unwrap();
            data.contains(&cantons)
        };
        // Don't waste time computing routes for small solutions or solutions already tried
        if cantons.len() > 10 && !is_tried {
            {
                let mut data = tried_solutions.lock().unwrap();
                data.insert(cantons.clone());
            }
            let all_cantons: HashSet<CantonId> = ctx.cantons.iter().map(|c| c.id).collect();
            match ctx.find_route_through_cantons(&cantons) {
                Some(route) => {
                    let visited_cantons: HashSet<CantonId> = cantons.iter().map(|c| *c).collect();
                    let missing_cantons: HashSet<CantonId> = all_cantons
                        .difference(&visited_cantons)
                        .map(|c| *c)
                        .collect();
                    Some(Arc::new(Individual {
                        legs: route.legs,
                        visited_cantons: cantons.clone(),
                        missing_cantons,
                        duration: route.arrival_time,
                    }))
                }
                None => None,
            }
        } else {
            None
        }
    }

    pub fn create_new_ones_with_mutations(
        &self,
        ctx: &Context,
        tried_solutions: &Mutex<HashSet<Vec<CantonId>>>,
        number_of_childs: usize,
    ) -> Vec<Arc<Self>> {
        let mut childs: Vec<Arc<Self>> = Vec::new();
        let mut rng = rand::thread_rng();

        let mut cantons_to_add: Vec<&CantonId> = self.missing_cantons.iter().collect();
        cantons_to_add.shuffle(&mut rng);
        cantons_to_add.truncate(number_of_childs / 2);

        for &canton_to_add in &cantons_to_add {
            let mut possible_position_to_insert: Vec<usize> = Vec::new();
            for (pos, canton) in self.visited_cantons.iter().enumerate() {
                if ctx.cantons_neighbors[&canton].contains(canton_to_add) {
                    possible_position_to_insert.push(pos + 1);
                }
            }
            // We can always insert at the end
            if !possible_position_to_insert.contains(&self.visited_cantons.len()) {
                possible_position_to_insert.push(self.visited_cantons.len());
            }

            let position_to_insert: usize = *possible_position_to_insert
                .iter()
                .choose(&mut rng)
                .expect("position to insert");
            let mut new_cantons_order: Vec<CantonId> = self.visited_cantons.clone();
            new_cantons_order.insert(position_to_insert, *canton_to_add);
            match Individual::generate_from_cantons_order(ctx, tried_solutions, new_cantons_order) {
                Some(child) => childs.push(child),
                _ => (),
            }
        }

        for _ in 0..(number_of_childs - cantons_to_add.len()) {
            if self.visited_cantons.len() <= 10 {
                break;
            }

            let (pos_to_remove, canton_to_reposition) = self
                .visited_cantons
                .iter()
                .enumerate()
                .choose(&mut rng)
                .expect("extract canton");

            // Don't shuffle the 3 first cantons
            if pos_to_remove <= 2 {
                continue;
            }

            let mut new_cantons_order: Vec<CantonId> = self.visited_cantons.clone();
            new_cantons_order.remove(pos_to_remove);

            // Find the position at which the first neighbor of the canton to reposition is found.
            match new_cantons_order
                .iter()
                .enumerate()
                .find(|(_, canton)| ctx.cantons_neighbors[&canton].contains(canton_to_reposition))
                .map(|(pos, _)| pos + 1)
            {
                Some(insertion_can_start_at_pos)
                    if insertion_can_start_at_pos < new_cantons_order.len() =>
                {
                    match (insertion_can_start_at_pos..new_cantons_order.len()).choose(&mut rng) {
                        Some(position_to_insert) => {
                            new_cantons_order.insert(position_to_insert, *canton_to_reposition);
                            match Individual::generate_from_cantons_order(
                                ctx,
                                tried_solutions,
                                new_cantons_order,
                            ) {
                                Some(child) => childs.push(child),
                                _ => (),
                            }
                        }
                        _ => {
                            println!(" /!\\ Failed to find a position to insert (can start at {}, len {})", insertion_can_start_at_pos, new_cantons_order.len());
                        }
                    }
                }
                _ => {
                    new_cantons_order.push(*canton_to_reposition);
                    match Individual::generate_from_cantons_order(
                        ctx,
                        tried_solutions,
                        new_cantons_order,
                    ) {
                        Some(child) => childs.push(child),
                        _ => (),
                    }
                }
            }
        }

        childs
    }

    pub fn breed(
        &self,
        ctx: &Context,
        tried_solutions: &Mutex<HashSet<Vec<CantonId>>>,
        other: &Self,
    ) -> Vec<Arc<Self>> {
        let mut childrens: Vec<Arc<Self>> = Vec::new();
        let mut rng = rand::thread_rng();

        for &canton in &self.missing_cantons {
            match crate::algos::sequences::insert_seq(
                &mut rng,
                &self.visited_cantons,
                &other.visited_cantons,
                canton,
            ) {
                None => (),
                Some(new_order) => {
                    let new_order = crate::algos::sequences::remove_duplicates(new_order);
                    match Self::generate_from_cantons_order(ctx, tried_solutions, new_order) {
                        None => (),
                        Some(children) => childrens.push(children),
                    }
                }
            }
        }

        childrens
    }

    // Create new individuals by taking every remaining canton and trying to add it to every
    // possible position.
    //
    // This is usefull close to the end of the solving to aim quickly for remaining cantons.
    pub fn bruteforce_one_more_canton(
        &self,
        ctx: &Context,
        tried_solutions: &Mutex<HashSet<Vec<CantonId>>>,
    ) -> Vec<Arc<Self>> {
        let mut childrens: Vec<Arc<Self>> = Vec::new();
        for &canton in &self.missing_cantons {
            let candidates =
                crate::algos::sequences::insertion_candidates(&self.visited_cantons, canton);
            for candidate in candidates {
                match Self::generate_from_cantons_order(ctx, tried_solutions, candidate) {
                    None => (),
                    Some(children) => childrens.push(children),
                }
            }
        }

        childrens
    }

    pub fn with_names<'a>(&self, ctx: &'a Context) -> super::IndividualWithNames<'a> {
        super::IndividualWithNames {
            legs: self
                .legs
                .iter()
                .map(|l| super::LegWithNames {
                    from: ctx
                        .stops
                        .get(&l.from)
                        .map(|s| s.name.as_str())
                        .unwrap_or("UNKNOWN"),
                    to: ctx
                        .stops
                        .get(&l.to)
                        .map(|s| s.name.as_str())
                        .unwrap_or("UNKNOWN"),
                    arrival_time: l.arrival_time,
                    cantons: ctx
                        .visited_cantons(l.trip, l.from, l.to)
                        .iter()
                        .map(|c| {
                            ctx.cantons
                                .iter()
                                .find(|i| i.id == *c)
                                .unwrap()
                                .name
                                .as_str()
                        })
                        .collect(),
                })
                .collect(),
            visited_cantons: self
                .visited_cantons
                .iter()
                .map(|canton| {
                    ctx.cantons
                        .iter()
                        .find(|c| c.id == *canton)
                        .unwrap()
                        .name
                        .as_str()
                })
                .collect(),
            missing_cantons: self
                .missing_cantons
                .iter()
                .map(|canton| {
                    ctx.cantons
                        .iter()
                        .find(|c| c.id == *canton)
                        .unwrap()
                        .name
                        .as_str()
                })
                .collect(),
            duration: super::PTTime::new(self.duration),
        }
    }

    /// Compute the score of this Individual
    pub fn score(&self) -> u32 {
        self.duration + 3 * 3600 * self.missing_cantons.len() as u32
    }
}
