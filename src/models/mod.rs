use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Arc;

use crate::algos::dijkstra_through_cantons::find_best_route_through_cantons;

pub mod individual;

pub type CantonId = u64;
pub type StopId = u32;
pub type TripId = u64;

pub struct Canton {
    pub id: CantonId,
    pub name: String,
}

pub struct Stop {
    pub id: StopId,
    pub canton: CantonId,
    pub transfers: Vec<StopTransfer>,
    pub name: String,
}

pub struct StopTransfer {
    pub to: StopId,
    pub time: u32,
}

pub struct StopTime {
    pub arrival_time: u32,
    pub departure_time: u32,
    pub stop: Arc<Stop>,
}

pub struct Trip {
    pub id: TripId,
    pub stop_times: Vec<StopTime>,
}

pub struct TripAtStop {
    pub arrival_time: u32,
    pub departure_time: u32,
    pub trip: Arc<Trip>,
}

pub struct Context {
    pub cantons: Vec<Canton>,
    pub cantons_neighbors: HashMap<CantonId, HashSet<CantonId>>,
    pub trips_at_stop: HashMap<StopId, Vec<TripAtStop>>,
    pub trips: HashMap<TripId, Arc<Trip>>,
    pub stops: HashMap<StopId, Arc<Stop>>,
    pub all_stops: Vec<Arc<Stop>>,
}

impl Context {
    pub fn from_file(file: &str) -> Self {
        let file = std::fs::File::open(file).expect("open input data file");
        let message_reader =
            capnp::serialize::read_message(file, capnp::message::ReaderOptions::new())
                .expect("read capnp data to input data file");
        let input_data = message_reader
            .get_root::<crate::models_capnp::input_data::Reader>()
            .expect("parse input data");

        let cantons: Vec<Canton> = input_data
            .get_cantons()
            .expect("get cantons from input data")
            .iter()
            .map(|c| Canton {
                id: c.get_id(),
                name: c
                    .get_name()
                    .expect("canton's name")
                    .to_string()
                    .expect("name to string"),
            })
            .collect();

        let cantons_neighbors: HashMap<CantonId, HashSet<CantonId>> = input_data
            .get_cantons_neighbors()
            .expect("get cantons neighbors from input data")
            .iter()
            .map(|cn| {
                (
                    cn.get_canton(),
                    cn.get_neighbors().expect("neighbors").iter().collect(),
                )
            })
            .collect();

        let stops: HashMap<StopId, Arc<Stop>> = input_data
            .get_stops()
            .expect("get stops from input data")
            .iter()
            .map(|s| {
                (
                    s.get_id() as u32,
                    Arc::new(Stop {
                        id: s.get_id() as u32,
                        name: s.get_name().unwrap().to_string().unwrap(),
                        canton: s.get_canton(),
                        transfers: s
                            .get_transfers()
                            .unwrap()
                            .iter()
                            .map(|t| StopTransfer {
                                to: t.get_to() as u32,
                                time: t.get_time(),
                            })
                            .collect(),
                    }),
                )
            })
            .collect();

        let all_stops: Vec<Arc<Stop>> = stops.iter().map(|(_, s)| s.clone()).collect();

        let trips: Vec<Arc<Trip>> = input_data
            .get_trips()
            .unwrap()
            .iter()
            .map(|t| {
                Arc::new(Trip {
                    id: t.get_id(),
                    stop_times: t
                        .get_stop_times()
                        .unwrap()
                        .iter()
                        .map(|st| StopTime {
                            arrival_time: st.get_arrival_time(),
                            departure_time: st.get_departure_time(),
                            stop: stops[&(st.get_stop() as u32)].clone(),
                        })
                        .collect(),
                })
            })
            .collect();

        Context {
            cantons,
            cantons_neighbors,
            trips_at_stop: trips_at_stop(&trips),
            trips: trips.iter().map(|t| (t.id, t.clone())).collect(),
            stops,
            all_stops,
        }
    }

    /// Return a set of all cantons visited during a trip from a given stop to another stop
    pub fn visited_cantons(&self, trip_id: TripId, from: StopId, to: StopId) -> Vec<CantonId> {
        let trip = &self.trips[&trip_id];
        let mut cantons: Vec<CantonId> = Vec::new();
        let mut prev_canton: Option<CantonId> = None;
        for st in trip.stop_times.iter().skip_while(|st| st.stop.id != from) {
            let canton = st.stop.canton;
            match prev_canton {
                Some(pc) if pc == canton => (),
                _ => {
                    cantons.push(canton);
                    prev_canton = Some(canton);
                }
            }

            if st.stop.id == to {
                break;
            }
        }
        cantons
    }

    pub fn find_route_through_cantons(&self, cantons: &Vec<CantonId>) -> Option<Route> {
        find_best_route_through_cantons(
            &self.trips_at_stop,
            &self.all_stops,
            cantons,
        )
    }
}

pub fn trips_at_stop(trips: &[Arc<Trip>]) -> HashMap<StopId, Vec<TripAtStop>> {
    let mut trips_at_stop: HashMap<StopId, Vec<TripAtStop>> = HashMap::new();

    for trip in trips {
        for stoptime in &trip.stop_times {
            let trip_at_stop = TripAtStop {
                arrival_time: stoptime.arrival_time,
                departure_time: stoptime.departure_time,
                trip: trip.clone(),
            };
            match trips_at_stop.get_mut(&stoptime.stop.id) {
                Some(trips_at_stop) => {
                    trips_at_stop.push(trip_at_stop);
                }
                None => {
                    trips_at_stop.insert(stoptime.stop.id, vec![trip_at_stop]);
                }
            }
        }
    }

    trips_at_stop
}

#[derive(Debug, PartialEq)]
pub struct Leg {
    pub from: StopId,
    pub to: StopId,
    pub arrival_time: u32,
    pub trip: TripId,
}

/// A route from two stops
#[derive(PartialEq, Debug)]
pub struct Route {
    pub dst: StopId,
    pub arrival_time: u32,
    pub legs: Vec<Arc<Leg>>,
}

pub struct LegWithNames<'a> {
    pub from: &'a str,
    pub to: &'a str,
    pub arrival_time: u32,
    pub cantons: Vec<&'a str>,
}

impl<'a> std::fmt::Display for LegWithNames<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Leg{{ {} -> {} at {} (via {}) }}",
            self.from,
            self.to,
            PTTime::new(self.arrival_time),
            (&self.cantons).join(", ")
        )?;
        Ok(())
    }
}

pub struct PTTime {
    secs: u32,
}

impl PTTime {
    pub fn new(secs: u32) -> PTTime {
        PTTime { secs }
    }
}

impl std::fmt::Display for PTTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let h = self.secs / 3600;
        let m = (self.secs % 3600) / 60;
        let s = self.secs % 60;
        write!(f, "{}:{}:{}", h, m, s)
    }
}

pub struct IndividualWithNames<'a> {
    pub legs: Vec<LegWithNames<'a>>,
    pub visited_cantons: Vec<&'a str>,
    pub missing_cantons: Vec<&'a str>,
    pub duration: PTTime,
}

impl<'a> std::fmt::Display for IndividualWithNames<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "IndividualWithNames{{\n")?;
        write!(f, " * duration: {}\n", self.duration)?;
        write!(
            f,
            " * visited {} cantons: {}\n",
            self.visited_cantons.len(),
            self.visited_cantons.join(", ")
        )?;
        write!(
            f,
            " * missing {} cantons: {}\n",
            self.missing_cantons.len(),
            self.missing_cantons.join(", ")
        )?;
        write!(f, " * legs:\n")?;
        for leg in &self.legs {
            write!(f, "   - {}\n", leg)?;
        }
        write!(f, "}}")?;
        Ok(())
    }
}
