fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let path = "resources/reduced-gtfs.zip";
    let path = "resources/gtfs.zip";
    let gtfs = gtfs_structures::Gtfs::from_path(path)?;
    gtfs.print_stats();
    println!("Loaded gtfs in {} ms", gtfs.read_duration);
    Ok(())
}
