fn main() {
    ::capnpc::CompilerCommand::new()
        .file("schemas/models.capnp")
        .run()
        .expect("compiling schema");
}
