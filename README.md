Try to solve the [Swiss Train
Challenge](https://actu.epfl.ch/news/an-algorithm-calculates-the-best-way-to-visit-26-c/):

> The goal is to set foot in each of Switzerland’s 26 cantons in less than 24 hours – relying solely on the country’s public transport network.

Usage
=====

Start with a preprocessing phase that will download required data and
preprocess it:

    cargo run --release --bin prepare

Then solve the challenge:

    cargo run --release --bin solve


Debian preparation
==================

To run the code on a debian machine:

    apt install curl capnproto libproj-dev pkg-config clang unzip tmux htop
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
